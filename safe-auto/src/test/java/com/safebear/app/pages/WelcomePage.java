package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by cca_student on 06/11/2017.
 */
public class WelcomePage {
    WebDriver driver;
    @FindBy(linkText = "Login")
    WebElement loginLink1;
    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    WebElement loginLink2;



    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public boolean clickOnLogin (LoginPage loginPage)
    {loginLink2.click();
    return loginPage.checkCorrectPage();
    }
}