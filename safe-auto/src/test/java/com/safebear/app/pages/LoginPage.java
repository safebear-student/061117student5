package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

/**
 * Created by cca_student on 06/11/2017.
 */
public class LoginPage {
    WebDriver driver;
    @FindBy(id = "myid")
    WebElement username_field;

    @FindBy(id = "mypass")
    WebElement password_field;

    @FindAll({
            @FindBy(tagName = "input")
    })
    List<WebElement> input_fields;


    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign In");
    }

    public void username() {
        if (input_fields.size() == 4 ) {
            input_fields.get(1).sendKeys("Hello");
        }
        else {
            System.out.println("found more than 4 fields!");
        }
    }

    public boolean login (UserPage userpage, String username, String password){
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
        return userpage.checkCorrectPage();
    }
}
